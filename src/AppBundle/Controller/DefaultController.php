<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Contact;
use AppBundle\Form\Type\ContactFormType;
use AppBundle\Lib\Utility;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use ReCaptcha\ReCaptcha;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
       
        try {
            $formModel = new Contact();
        } catch (\Exception $exception) {
            throw new \RuntimeException('an error occurred');
        }
        $form = $this->createForm(ContactFormType::class, $formModel);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid() && $this->captchaverify($request->get('g-recaptcha-response'))) {
            $em      = $this->getDoctrine()->getManager();
            $contact = $form->getData();
            $em->persist($contact);
            $em->flush();

            return new redirectToRoute('homepage');

        } elseif ($form->isSubmitted()) {
            return new JsonResponse([
                'status'   => 'error',
                'errors'   => Utility::getErrorsFromForm($form),
            ]);
        }
        # check if captcha response isn't get throw a message
        if($form->isSubmitted() &&  $form->isValid() && !$this->captchaverify($request->get('g-recaptcha-response'))){
                 
            $this->addFlash(
                'error',
                'Captcha Require'
              );             
            }
            
        return $this->render('@App/form.html.twig', array(
            'form' => $form->createView()  
        ));
    }
    
    # get success response from recaptcha and return it to controller
    function captchaverify($recaptcha){
            $url = "https://www.google.com/recaptcha/api/siteverify";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); 
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, array(
                "secret"=>"6Lc_43IUAAAAAH5xtMMT5b2p0BWLVw6tu5_Jg8sX","response"=>$recaptcha));
            $response = curl_exec($ch);
            curl_close($ch);
            $data = json_decode($response);     
        
        return $data['success'];        
    }
      
}
